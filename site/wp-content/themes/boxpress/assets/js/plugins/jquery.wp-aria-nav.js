(function ($) {
  'use strict';

  /**
   * WP Aria Navigations
   * ---
   * Use in conjunction with WP Aria Menu Walker for a
   * more accessible navigation. Heavily uses timers to
   * gage mouse intent.
   * 
   * Features:
   * - Toggle 'aria-expanded' attribute on focus and hover
   * - Display sub-menu on tab focus
   * - Fixed Sub-Menu - Sub-menu displays fixed on 'moutseout' or
   *   until user clicks out
   * - Hover Intent like feature - Don't enable the fixed sub-menu
   *   until mouse stays on menu item for extended time
   * - Optional Overlay - Set the overlay paramater to true to enable
   *
   * Todo:
   * - Inline CSS
   */

  $.fn.wpAriaNav = function ( options ) {
    options = jQuery.extend({
      overlay         : true,
      overlayClass    : 'js-aria-hover-overlay',
      menuHoverClass  : 'is-active',
      mouseOutTimeout : 0,
      mouseDelay      : 350,
    }, options );

    var $aria_nav         = $(this);
    var $nav_top_item     = $aria_nav.find('> ul > li > a');
    var $aria_dropdown    = $aria_nav.find('li[aria-haspopup="true"]');
    var $aria_nav_overlay = '';
    var has_overlay       = false;

    var unset_active_delay = 0;
    var mouseleave_timer;
    var delay_timer;
    var first_leave_timer;
    var is_first_leave = false;

    if ( options.overlayClass !== '' && options.overlay === true ) {
      has_overlay = true;
    }


    /**
     * Close first and last sub-navs when tabbing out of nav
     */

    var $first_focusable_link = $aria_nav.find('> ul > li:first-child > a');
    var $last_focusable_link  = $aria_nav.find('> ul > li:last-child > a');

    var $last_nav_ul = $aria_nav.find('> ul > li:last-child').children( 'ul' );
    var $next_nav_ul = $last_nav_ul;

    // Loop through all child ULs until we get the last one
    if ( $last_nav_ul.length ) {
      while ( $next_nav_ul.length ) {
        $last_nav_ul = $next_nav_ul;
        $next_nav_ul = $next_nav_ul.children( 'li:last-child' ).children( 'ul' );
      }

      $last_focusable_link = $last_nav_ul.find( 'li:last-child > a' );
    }

    // Close sub-nav when tabbing off of first item
    $first_focusable_link.on( 'keydown', function ( e ) {
      if ( e.which === 9 && e.shiftKey ) {
        aria_unset_active( $aria_dropdown );
      }
    });

    // Close sub-nav when tabbing off of last item
    $last_focusable_link.on( 'keydown', function ( e ) {
      if ( e.which === 9 && ! e.shiftKey ) {
        aria_unset_active( $aria_dropdown );
      }
    });


    /**
     * Add overlay if it's enabled
     */

    if ( has_overlay ) {
      $aria_nav.after('<div class="' + options.overlayClass + '"></div>');
      $aria_nav_overlay = $aria_nav.next( '.' + options.overlayClass );

      // Unset active when overlay is clicked
      $aria_nav_overlay.on('click', function () {
        aria_unset_active( $aria_dropdown );
      });
    }


    // Unset all active Aria items and activate the current
    $aria_dropdown.on('focusin mouseenter', function ( e ) {
      var $this = $(this);
      var $focused_link = $( ':focus' );
      var mouse_delay = options.mouseDelay;

      // Unset active if another item that does not have a child is focused
      if ( ! $focused_link.closest( '.sub-menu .sub-menu' ).length &&
           ! $focused_link.parent().hasClass( 'menu-item-has-children' )) {
        aria_unset_active( $aria_dropdown );
      }


      // Reset our timeout when mouse re-enters after
      // triggering a mouseleave
      clearTimeout( mouseleave_timer );

      // Enable active state
      $this.attr( 'aria-expanded', 'true' )
        .addClass( options.menuHoverClass );

      if ( has_overlay ) {
        $aria_nav_overlay.addClass( options.menuHoverClass );
      }


      // Prevent immediate close if we re-enter the menu quickly
      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }


      /**
       * Ignore mouse if it's 'just passing through'
       * ---
       * `unset_active_delay` default is 0 and is only updated if
       * the mouse is sticking around
       */

      delay_timer = setTimeout(function () {
        unset_active_delay = options.mouseOutTimeout;
      }, mouse_delay );

      e.preventDefault();
    });


    // Unset all active items
    $nav_top_item.on('focusin mouseenter', function (e) {
      aria_unset_active( $aria_dropdown );

      e.preventDefault();
    });


    // Don't close dropdowns right away when mouse leaves
    $aria_dropdown.on('mouseleave', function () {

      /**
       * Delay sub-nav closing
       */

      mouseleave_timer = setTimeout(function () {
        aria_unset_active( $aria_dropdown );
      }, unset_active_delay );


      /**
       * Reset 'Just passing through' Timer
       */

      unset_active_delay = 0;
      clearTimeout( delay_timer );


      /**
       * Track if mouse re-enters quickly
       * ---
       * Bypasses the 'Just passing through' timer to allow
       * sub-nav to stick around if mouse accidentally leaves
       * and comes back quickly
       */

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, options.mouseDelay );

    });


    // Change Aria status and remove active class
    function aria_unset_active( $aria_dropdown ) {
      $aria_dropdown.attr( 'aria-expanded', 'false' )
        .removeClass( options.menuHoverClass );

      if ( has_overlay ) {
        $aria_nav_overlay.removeClass( options.menuHoverClass );
      }
    }
  };

})(jQuery);
