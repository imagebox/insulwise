(function ($) {
  'use strict';

  /**
   * Site Carousels
   * ===
   */

  /**
   * Hero Carousel
   * ---
   * Tiny Slider
   * [https://github.com/ganlanyuan/tiny-slider#options]
   */

  var sliders = document.querySelectorAll( '.js-carousel' );
  var slider_init;

  if ( sliders.length ) {
    sliders.forEach(function (slider) {
      slider_init = tns({
        container: slider,
        mouseDrag: true,
        speed: 1500,
        autoplayTimeout: 7500,
        autoplay: true,
        navAsThumbnails: true,
        navContainer: "#js-carousel-thumbs",
        controlsText: [
          '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon"/></svg>',
          '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon"/></svg>',
        ]
      });
    });
  }

  var $hero_carousel = $('.js-carousel-hero');
  var hero_carousel_init;

  if ( $hero_carousel.length ) {
    hero_carousel_init = tns({
      container: '.js-carousel-hero',
      mouseDrag: true,
      speed: 1000,
      autoplayTimeout: 9000,
      autoplay: true,
      controlsText: [
        '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon"/></svg>',
        '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon"/></svg>',
      ]
    });
  }

  var $logo_carousel = $('.logo-carousel');
  var logo_carousel_init;

  if ( $logo_carousel.length ) {
    logo_carousel_init = tns({
      container: '.logo-carousel',
      mouseDrag: true,
      speed: 800,
      autoplay: true,
      responsive: {
         0 : {
           items: 1,
         },
         480 : {
           items: 3,
         },
         1200: {
           items: 5,
         }
      },
      controlsText: [
        '<span class="vh">Previous</span><svg class="svg-left-icon" width="40" height="40" focusable="false"><use href="#arrow-left-icon-2"/></svg>',
        '<span class="vh">Next</span><svg class="svg-right-icon" width="40" height="40" focusable="false"><use href="#arrow-right-icon-2"/></svg>',
      ]
    });
  }

})(jQuery);
