(function ($) {
  'use strict';

  /**
   * Site javascripts
   * ===
   */

  // Wrap video embeds in Flexible Container
  $('iframe[src*="youtube.com"]:not(.not-responsive), iframe[src*="vimeo.com"]:not(.not-responsive)').wrap('<div class="flexible-container"></div>');


      $('.slide-video-button').magnificPopup({type: 'iframe'});
      $('.card-video-button').magnificPopup({type: 'iframe'});
      $('.shortcut-video-button').magnificPopup({type: 'iframe'});

  $('.js-testimonial-popup').magnificPopup({ type: 'inline' });



  $('.first, .drop--down-1').hover(
    function () {
      $('.drop--down-1').show();
    },
    function () {
      $('.drop--down-1').hide();
    }
  );

  $('.second, .drop--down-2').hover(
    function () {
      $('.drop--down-2').show();
    },
    function () {
      $('.drop--down-2').hide();
    }
  );

})(jQuery);
