<?php
/**
 * Template footer
 *
 * Contains the closing of the main el and all content after.
 *
 * @package boxpress
 */
?>
</main>
<footer id="colophon" class="site-footer" role="contentinfo">
  <div class="wrap">
    <div class="primary-footer">
      <div class="l-footer l-footer--2-cols l-footer--gap-large">
        <div class="l-footer-item">
          <?php get_template_part( 'template-parts/global/address-block' ); ?>
        </div>
        <div class="l-footer-item l-footer-item--pull-right">
          <?php get_template_part( 'template-parts/global/social-nav' ); ?>
        </div>
      </div>
    </div>
    <div class="site-info">
      <div class="l-footer l-footer--3-cols l-footer--align-center">
        <div class="l-footer-item">
          <div class="site-copyright">
            <p>
              <small>
                <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                <?php
                  $company_name     = get_bloginfo( 'name', 'display' );
                  $alt_company_name = get_field( 'alternative_company_name', 'option' );

                  if ( ! empty( $alt_company_name )) {
                    $company_name = $alt_company_name;
                  }
                ?>
                <?php echo $company_name; ?>.
                <?php _e('', 'boxpress'); ?>
              </small>
            </p>
          </div>
        </div>
        <?php // Footer Navigation ?>
        <?php if ( has_nav_menu( 'footer' )) : ?>
          <div class="l-footer-item">
            <nav class="navigation--footer"
              aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
              role="navigation">
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'footer',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          </div>
        <?php endif; ?>
        <div class="l-footer-item l-footer-item--pull-right">
          <div class="imagebox">
            <p>
              <small>
                <?php _e('Website by', 'boxpress'); ?>
                <a href="https://imagebox.com" target="_blank">
                  <span class="vh">Imagebox</span>
                  <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                    <use href="#imagebox-logo"/>
                  </svg>
                </a>
              </small>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-rainbow">
    <div class="rainbow red-footer"></div>
    <div class="rainbow orange-footer"></div>
    <div class="rainbow yellow-footer"></div>
    <div class="rainbow green-footer"></div>
  </div>
</footer>
</div>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
