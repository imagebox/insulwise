<?php
/**
 * Register site navigations
 *
 * @package boxpress
 */

function boxpress_register_theme_navs() {
  register_nav_menus( array(
    'primary'    => __( 'Primary Menu', 'boxpress' ),
    'secondary'  => __( 'Secondary Menu', 'boxpress' ),
    'footer'     => __( 'Footer Menu', 'boxpress' ),
    'choose_your_house'     => __( 'Choose Home Drop Menu', 'boxpress' ),
    'insulation'     => __( 'Insulation Drop Menu', 'boxpress' ),
  ));
}
add_action( 'after_setup_theme', 'boxpress_register_theme_navs' );
