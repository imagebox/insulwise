<?php
/**
 * WordPress admin related functions
 *
 * @package boxpress
 */


/**
 * BoxPress Logo
 */
function my_login_logo() { ?>
  <style type="text/css">
    body.login {
      font-size: 1em;
    }
    body.login div#login,
    body.login div#login h1 {
      font-size: inherit;
    }
    body.login div#login h1 a {
      background-image: url( '<?php echo get_stylesheet_directory_uri(); ?>/assets/img/dist/branding/boxpress.png' );
      padding-bottom: 1.875em;
      height: 3.125em;
      width: 10.875em;
      font-size: inherit;
      background-size: 10.875em, 3.125em;
      transition: opacity 500ms;
    }
    body.login div#login h1 a:hover,
    body.login div#login h1 a:focus {
      opacity: 0.75;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


/**
 * Show the kitchen sink by default
 */
function unhide_kitchensink( $args ) {
  $args['wordpress_adv_hidden'] = false;
    return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );


/**
 * Set default media link to 'none'
 */
update_option('image_default_link_type','none');


/**
 * Set default gallery link to 'file'
 */

function my_gallery_default_type_set_link( $settings ) {
  $settings['galleryDefaults']['link'] = 'file';
  return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');


/**
 * Remove 'Editor' from Appearance menu
 */
function remove_editor_menu() {
  remove_action('admin_menu', '_add_themes_utility_last', 101);
}
add_action('_admin_menu', 'remove_editor_menu', 1);


/**
 * Remove CSS section from Appearance menu
 * @param $wp_customize WP_Customize_Manager
 */
function prefix_remove_css_section( $wp_customize ) {
  $wp_customize->remove_section( 'custom_css' );
}
add_action( 'customize_register', 'prefix_remove_css_section', 15 );


/**
 * Add User Role Class to Body
 */
function print_user_classes() {
  if ( is_user_logged_in() ) {
    add_filter('body_class', 'class_to_body');
  }
}
add_action('init', 'print_user_classes');

// Add user role class and user id to body tag
function class_to_body( $classes ) {
  if ( is_user_logged_in() ) {
    $current_user = wp_get_current_user();
    $user_roles   = $current_user->roles;
    $user_ID      = $current_user->ID;

    // Add user ID Class
    $classes[] = "user-id-{$user_ID}";

    if ( $user_roles ) {
      foreach ( $user_roles as $role ) {
        // Add user role Classes
        $classes[] = "user-role-{$role}";
      }
    }

    return $classes;
  }
}
