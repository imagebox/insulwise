<?php
/**
 * Displays the page template
 *
 * @package boxpress
 */

$child_pages_list = query_for_child_page_list();

?>
<?php get_header(); ?>



  <section class="fullwidth-column section">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'page' ); ?>
          <?php endwhile; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>


          <div class="l-aside-col">
            <?php get_sidebar('ip'); ?>
          </div>

      </div>

    </div>
  </section>

<?php get_footer(); ?>
