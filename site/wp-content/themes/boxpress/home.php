<?php
/**
 * The template for displaying the Blog page.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package boxpress
 */

get_header(); ?>


  <section class="section blog-page">
    <div class="wrap">

      <div class="l-sidebar">
        <div class="l-main-col">

          <?php if ( have_posts() ) : ?>


            <?php while ( have_posts() ) : the_post(); ?>

              <?php get_template_part( 'template-parts/content/content-preview' ); ?>

            <?php endwhile; ?>

            <?php the_posts_navigation(); ?>

          <?php else : ?>

            <?php get_template_part( 'template-parts/content/content', 'none' ); ?>

          <?php endif; ?>

        </div>
        <div class="l-aside-col">

          <?php get_sidebar(); ?>

        </div>
      </div>
    </div>
  </section>

<?php get_footer(); ?>
