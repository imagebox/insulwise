<?php
/**
 * Template header
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @package boxpress
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">

  <?php wp_head(); ?>

  <?php // Header Tracking Codes ?>
  <?php the_field( 'header_tracking_codes', 'option' ); ?>

  <script>
    // No js Fallback - Remove 'no-js', add 'js' to html el
    !function(){var n=document.documentElement.classList;n.contains('no-js')&&(n.remove('no-js'),n.add('js'));}();
  </script>
</head>
<body <?php body_class(); ?>>
<?php the_field( 'header_tracking_codes_after', 'option' ); ?>

<?php // Skip Link ?>
<a class="site-skip-link" href="#main"><?php _e( 'Skip to main content', 'boxpress' ); ?></a>

<?php // SVGs ?>
<?php include( get_template_directory() . '/template-parts/global/svg.php' ); ?>

<?php // Mobile Header ?>
<?php include( get_template_directory() . '/template-parts/global/header-mobile.php' ); ?>

<div class="site-wrap">
<header id="masthead" class="site-header" role="banner">

  <?php if ( have_rows( 'alert_bar' )) : ?>
    <?php while ( have_rows( 'alert_bar' )) : the_row();

    $alert_link = get_sub_field('alert_link');
    $alert_option = get_sub_field('alert_option');

    ?>
      <div class="alert-bar <?php echo $alert_option; ?>">
         <a
           href="<?php echo esc_url( $alert_link['url'] ); ?>"
           target="<?php echo esc_attr( $alert_link['target'] ); ?>">
           <?php echo $alert_link['title']; ?>
         </a>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

  <div class="wrap">

    <div class="l-header">
      <div class="l-header-col-1">

        <div class="site-branding">
          <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/src/branding/insulwise-fullcolor.png" alt="Insulwise Logo" />
          </a>
        </div>
        <p>The Pittsburgh Home Insulation Experts!</p>

      </div>
      <div class="l-header-col-2">

        <div class="site-navigation">

          <?php // Utility Navigation ?>
          <?php if ( has_nav_menu( 'secondary' )) : ?>
            <nav class="navigation--utility"
              aria-label="<?php _e( 'Utility Navigation', 'boxpress' ); ?>"
              role="navigation">
              <ul class="nav-list">
                <?php
                  wp_nav_menu( array(
                    'theme_location'  => 'secondary',
                    'items_wrap'      => '%3$s',
                    'container'       => false,
                    'walker'          => new Aria_Walker_Nav_Menu(),
                  ));
                ?>
              </ul>
            </nav>
          <?php endif; ?>

    <?php if ( have_rows( 'site_offices', 'option' )) : ?>

    <address>

      <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
        <?php
          $phone_number   = get_sub_field('phone_number');
        ?>

        <?php if ( ! empty( $phone_number ) || ! empty( $email )) : ?>

          <div class="address-contact-info">
            <h6>Call Us Today</h6>
            <?php if ( ! empty( $phone_number )) : ?>
              <?php
                // Strip hyphens & parenthesis for tel link
                $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
              ?>
              <p>
                <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
                <a href="tel:+1<?php echo $tel_formatted; ?>">
                  <span itemprop="telephone"><?php echo $phone_number; ?></span>
                </a>
              </p>
            <?php endif; ?>
          </div>

        <?php endif; ?>
      <?php endwhile; ?>

    </address>

  <?php endif; ?>

        </div>

      </div>
    </div>

  </div>

    <nav class="black">
      <div class="wrap">
        <?php // Main Navigation ?>
        <?php if ( has_nav_menu( 'primary' )) : ?>
          <nav class="js-accessible-menu navigation--main"
            aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
            role="navigation">

            <ul class="nav-list">

              <?php
                wp_nav_menu( array(
                  'theme_location'  => 'primary',
                  'items_wrap'      => '%3$s',
                  'container'       => false,
                  'walker'          => new Aria_Walker_Nav_Menu(),
                ));
              ?>
            </ul>
          <?php endif; ?>
      </div>
    </nav>
  </nav>


  <?php // Drop down ?>
  <?php if ( has_nav_menu( 'choose_your_house' )) : ?>
    <nav class="js-accessible-menu choose-house drop--down  drop--down-1"
      aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
      role="navigation">
      <ul class="nav-list">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'choose_your_house',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    </nav>
<?php endif; ?>

  <?php // Drop down ?>
  <?php if ( has_nav_menu( 'insulation' )) : ?>
    <nav class="js-accessible-menu choose-house drop--down  drop--down-2"
      aria-label="<?php _e( 'Main Navigation', 'boxpress' ); ?>"
      role="navigation">
      <ul class="nav-list">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'insulation',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    </nav>
<?php endif; ?>


</header>


<main id="main" class="site-main" role="main">
  <?php if ( have_rows( 'alert_bar' )) : ?>
    <?php while ( have_rows( 'alert_bar' )) : the_row();

    $alert_link = get_sub_field('alert_link');
    $alert_option = get_sub_field('alert_option');

    ?>
      <div class="alert-bar <?php echo $alert_option; ?>">
         <a
           href="<?php echo esc_url( $alert_link['url'] ); ?>"
           target="<?php echo esc_attr( $alert_link['target'] ); ?>">
           <?php echo $alert_link['title']; ?>
         </a>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
