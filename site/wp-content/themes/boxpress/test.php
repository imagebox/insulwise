








<?php if ( $slide_link ) : ?>

  <div class="slide-footer">
    <div class="slide-thumbnail">
      <a class="slide-video-button" href="<?php echo esc_url( $slide_link ); ?>">
        <img draggable="false" aria-hidden="true"
          src="<?php echo esc_url( $slide_link_photo['sizes'][ $slide_link_photo_size ] ); ?>"
          width="<?php echo esc_attr( $slide_link_photo['sizes'][ $slide_link_photo_size . '-width'] ); ?>"
          height="<?php echo esc_attr( $slide_link_photo['sizes'][ $slide_link_photo_size . '-height'] ); ?>"
          alt="">
        <svg class="thumb-play-button-svg" width="56" height="56">
          <use href="#thumbnail-play-button" />
        </svg>
      </a>
    </div>

    <a class="button slide-video-button" href="<?php echo esc_url( $slide_link ); ?>">
      <?php _e( 'Watch Live Animation', 'boxpress' ); ?>
    </a>
  </div>

<?php elseif ( $slide_link_2 ) : ?>

  <div class="slide-footer">
    <a class="button no-thumb" href="<?php echo esc_url( $slide_link_2['url'] ); ?>" target="<?php echo esc_attr( $slide_link_2['target'] ); ?>">
      <?php echo $slide_link_2['title']; ?>
    </a>
  </div>

<?php endif; ?>

</div>
</div>
</div>

<?php endwhile; ?>
