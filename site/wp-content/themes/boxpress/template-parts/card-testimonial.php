<?php
/**
 * Displays a Testimonial item and popup.
 *
 * @package boxpress
 */

$testimonial_thumb            = get_field('testimonial_thumb');
$testimonial_opening_quote    = get_field('testimonial_opening_quote');
$testimonial_location         = get_field('testimonial_location');
$testimonial_full_review_link = get_field('testimonial_full_review_link');
$testimonial_case_study       = get_field('testimonial_case_study');
$testimonial_review_logo      = get_field('testimonial_review_logo');
$testimonial_review_grade     = get_field('testimonial_review_grade');

?>
<div class="testimonial-preview">
    
  <?php if ( $testimonial_thumb ) : ?>
    <div class="testimonial-figure">
      <img src="<?php echo $testimonial_thumb['url']; ?>"
        width="<?php echo $testimonial_thumb['width']; ?>"
        height="<?php echo $testimonial_thumb['height']; ?>"
        alt="<?php echo $testimonial_thumb['alt']; ?>">
    </div>
  <?php endif; ?>

  <div class="testimonial-body">
    <?php if ( ! empty( $testimonial_opening_quote )) : ?>
      <h2><?php echo $testimonial_opening_quote; ?></h2>
    <?php endif; ?>

    <?php if ( '' !== get_post()->post_content ) : ?>
      <div class="testimonial-excerpt">
        <?php the_excerpt(); ?>
      </div>
    <?php endif; ?>

    <a href="#popup-<?php echo get_the_ID(); ?>" class="js-testimonial-popup"><?php _e( 'Read More', 'boxpress' ); ?> ></a>

    <ul class="testimonial-meta">
      <li class="testimonial-meta__name"><?php the_title(); ?></li>
      
      <?php if ( ! empty( $testimonial_location )) : ?>
        <li class="testimonial-meta__location">
          <?php echo $testimonial_location; ?>
        </li>
      <?php endif; ?>
    </ul>

    <?php if ( $testimonial_review_logo || ! empty( $testimonial_review_grade )) : ?>
      <div class="testimonial-review-info">

        <?php if ( $testimonial_review_logo ) : ?>
          <img src="<?php echo $testimonial_review_logo['url']; ?>"
            width="<?php echo $testimonial_review_logo['width']; ?>"
            height="<?php echo $testimonial_review_logo['height']; ?>"
            alt="<?php echo $testimonial_review_logo['alt']; ?>">
        <?php endif; ?>

        <?php if ( ! empty( $testimonial_review_grade )) : ?>
          <p><?php echo $testimonial_review_grade; ?></p>
        <?php endif; ?>

      </div>
    <?php endif; ?>
    
  </div>
</div>

<div class="testimonial-popup" style="display: none;">
  <div id="popup-<?php echo get_the_ID(); ?>" class="testimonial-full">

    <?php if ( $testimonial_thumb ) :
        $size = 'testimonial-thumb';
        $testimonial_thumb_url    = $testimonial_thumb['url'];
        $testimonial_thumb_width  = $testimonial_thumb['width'];
        $testimonial_thumb_height = $testimonial_thumb['height'];
        $testimonial_thumb_alt    = $testimonial_thumb['alt'];
      ?>
      <div class="testimonial-figure">
        <img src="<?php echo $testimonial_thumb_url; ?>"
          width="<?php echo $testimonial_thumb_width; ?>"
          height="<?php echo $testimonial_thumb_height; ?>"
          alt="<?php echo $testimonial_thumb_alt; ?>">
      </div>
    <?php endif; ?>

    <div class="testimonial-body">
      <?php if ( ! empty( $testimonial_opening_quote )) : ?>
        <h2>
          <?php echo $testimonial_opening_quote; ?>
        </h2>
      <?php endif; ?>

      <div class="testimonial-full-content">
        <?php the_content(); ?>

        <ul class="testimonial-meta">
          <li class="testimonial-meta__name"><?php the_title(); ?></li>
          
          <?php if ( ! empty( $testimonial_location )) : ?>
            <li class="testimonial-meta__location">
              <?php echo $testimonial_location; ?>
            </li>
          <?php endif; ?>
        </ul>


        <div class="testimonial-content-footer cf">

          <?php if ( $testimonial_review_logo || ! empty( $testimonial_review_grade )) : ?>
            <div class="testimonial-review-info">

              <?php if ( $testimonial_review_logo ) : ?>
                <img src="<?php echo $testimonial_review_logo['url']; ?>"
                  width="<?php echo $testimonial_review_logo['width']; ?>"
                  height="<?php echo $testimonial_review_logo['height']; ?>"
                  alt="<?php echo $testimonial_review_logo['alt']; ?>">
              <?php endif; ?>

              <?php if ( ! empty( $testimonial_review_grade )) : ?>
                <p><?php echo $testimonial_review_grade; ?></p>
              <?php endif; ?>
            </div>
          <?php endif; ?>

          <?php if ( ! empty( $testimonial_full_review_link )) : ?>
            <p><a class="button" href="<?php echo $testimonial_full_review_link; ?>">View the Full Review</a></p>
          <?php endif; ?>

          <?php if ( ! empty( $testimonial_case_study )) : ?>
            <p><a class="button" href="<?php echo $testimonial_case_study; ?>">View the Case Study</a></p>
          <?php endif; ?>
        </div>

      </div>

    </div>
  </div>
</div>
