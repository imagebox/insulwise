<?php
/**
 * Mobile Header
 *
 * @package boxpress
 */
?>
<header class="site-header--mobile" role="banner">
  <div class="mobile-header-left">
    <div class="site-branding">
      <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
        <span class="vh"><?php bloginfo('name'); ?></span>
        <a href="<?php echo esc_url( home_url( '/' )); ?>" rel="home">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/img/src/branding/insulwise-fullcolor.png" alt="Insulwise Logo" />
        </a>
      </a>
    </div>
      <p>The Pittsburgh Home Insulation Experts!</p>
  </div>
  <div class="mobile-header-right">
    <button type="button" class="js-toggle-nav menu-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Menu', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="48" height="33" focusable="false">
        <use href="#menu-icon"/>
      </svg>
    </button>
  </div>
</header>

<div id="mobile-nav-tray" class="mobile-nav-tray" aria-hidden="true">
  <div class="mobile-nav-header">
    <button type="button" class="js-toggle-nav close-button"
      aria-controls="mobile-nav-tray"
      aria-expanded="false">
      <span class="vh"><?php _e('Close', 'boxpress'); ?></span>
      <svg class="menu-icon-svg" width="33" height="33" focusable="false">
        <use href="#close-icon"/>
      </svg>
    </button>
  </div>
  <nav class="navigation--mobile">

    <?php if ( has_nav_menu( 'primary' )) : ?>
      <ul class="mobile-nav mobile-nav--main">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'primary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>


    <?php if ( has_nav_menu( 'secondary' )) : ?>
      <ul class="mobile-nav mobile-nav--utility">
        <?php
          wp_nav_menu( array(
            'theme_location'  => 'secondary',
            'items_wrap'      => '%3$s',
            'container'       => false,
            'walker'          => new Aria_Walker_Nav_Menu(),
          ));
        ?>
      </ul>
    <?php endif; ?>
  </nav>

  <?php if ( have_rows( 'site_offices', 'option' )) : ?>

  <address>

    <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
      <?php
        $phone_number   = get_sub_field('phone_number');
      ?>

      <?php if ( ! empty( $phone_number ) || ! empty( $email )) : ?>

        <div class="address-contact-info">
          <h6>Call Us Today</h6>
          <?php if ( ! empty( $phone_number )) : ?>
            <?php
              // Strip hyphens & parenthesis for tel link
              $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
            ?>
            <p>
              <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
              <a href="tel:+1<?php echo $tel_formatted; ?>">
                <span itemprop="telephone"><?php echo $phone_number; ?></span>
              </a>
            </p>
          <?php endif; ?>
        </div>

      <?php endif; ?>
    <?php endwhile; ?>

  </address>

<?php endif; ?>
</div>
