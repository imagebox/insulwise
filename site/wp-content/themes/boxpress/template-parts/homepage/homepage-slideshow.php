<?php
/**
 * Displays the Slideshow layout
 *
 * @package boxpress
 */
?>
<?php if ( have_rows( 'home-carousel' )) : ?>

  <div class="home-carousel">
    <div class="js-carousel-hero owl-carousel owl-theme">

      <?php while ( have_rows( 'home-carousel' )) : the_row(); ?>
        <?php
          $slide_heading  = get_sub_field( 'slide_heading' );
          $slide_subhead  = get_sub_field( 'slide_subhead' );
          $slide_bkg      = get_sub_field( 'slide_bkg' );
          $slide_link      = get_sub_field( 'slide_link' );
          $slide_link_2      = get_sub_field( 'slide_link_2' );
          $slide_link_photo      = get_sub_field( 'slide_link_photo' );
          $slide_link_photo_size = 'slide_link_photo_size';
        ?>

        <div class="carousel-slide">

          <?php if ( $slide_bkg ) : ?>
            <?php
              $image_size = 'home_slideshow_big';
            ?>
            <img class="slide-bkg" draggable="false" aria-hidden="true"
              src="<?php echo $slide_bkg['url']; ?>"
              width="<?php echo $slide_bkg['width' ]; ?>"
              height="<?php echo $slide_bkg['height' ]; ?>"
              alt="">
          <?php endif; ?>

          <div class="slide-content-container">
            <div class="wrap slide-wrap">
              <div class="slide-content">

                <h2 class="h1 slide-title"><?php echo $slide_heading; ?></h2>
                <p class="slide-sub-title"><?php echo $slide_subhead; ?></p>



              </div>
              <?php if ( $slide_link ) : ?>

                <div class="slide-footer">
                  <div class="slide-thumbnail">
                    <a class="slide-video-button" href="<?php echo esc_url( $slide_link ); ?>">
                      <img draggable="false" aria-hidden="true"
                        src="<?php echo esc_url( $slide_link_photo['sizes'][ $slide_link_photo_size ] ); ?>"
                        width="<?php echo esc_attr( $slide_link_photo['sizes'][ $slide_link_photo_size . '-width'] ); ?>"
                        height="<?php echo esc_attr( $slide_link_photo['sizes'][ $slide_link_photo_size . '-height'] ); ?>"
                        alt="">
                      <svg class="thumb-play-button-svg" width="56" height="56">
                        <use href="#thumbnail-play-button" />
                      </svg>
                    </a>
                  </div>

                  <a class="button slide-video-button" href="<?php echo esc_url( $slide_link ); ?>">
                    <?php _e( 'Watch Live Animation', 'boxpress' ); ?>
                  </a>
                </div>

              <?php endif; ?>


                <?php if ( $slide_link_2 ) : ?>

                <div class="slide-footer">
                  <a class="button no-thumb" href="<?php echo esc_url( $slide_link_2['url'] ); ?>" target="<?php echo esc_attr( $slide_link_2['target'] ); ?>">
                    <?php echo $slide_link_2['title']; ?>
                  </a>
                </div>

              <?php endif; ?>

            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </div>

<?php endif; ?>
