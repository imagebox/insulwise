<?php
/**
 * Displays the page banner
 *
 * @package boxpress
 */

$banner_title     = get_the_title();
$banner_color = get_field( 'banner_color', get_the_ID() );
$banner_image_url = '';
$default_banner   = get_field( 'default_banner_image', 'option' );

if ( $default_banner ) {
  $banner_image_url = $default_banner['url'];
}



// Set top page title and banner image
if ( 0 !== $post->post_parent ) {
  $post_ancestors = get_post_ancestors( $post->ID );
  $post_id = end( $post_ancestors );
  $banner_title = get_the_title( $post_id );
  $top_featured_image = get_the_post_thumbnail_url( $post_id );
  $banner_color = get_field( 'banner_color', $post_id );


  if ( ! empty( $top_featured_image )) {
    $banner_image_url = $top_featured_image;
  }
}

if ( has_post_thumbnail() ) {
  $banner_image_url = get_the_post_thumbnail_url( get_the_ID() );
}

?>


<header class="banner <?php echo $banner_color; ?>">
  <div class="wrap">
    <div class="banner-title">
      <span class="h1">
        <?php echo $banner_title; ?>
      </span>
    </div>
    <?php if ( ! empty( $banner_image_url )) : ?>
      <div class="photo-container">
        <div class="banner-photo">
          <img class="banner-image" draggable="false" src="<?php echo $banner_image_url; ?>" alt="">
        </div>
      </div>
    <?php endif; ?>
  </div>
</header>
