<?php
/**
 * Template Name: Testimonials
 *
 * Displays the page Testimonials page template
 *
 * @package boxpress
 */
?>
<?php get_header(); ?>


  <section class="fullwidth-column section testimonial-section color-option-1">
    <div class="wrap">

      <?php if ( have_posts() ) : ?>
        <div class="testimonials-header">
          <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
          <?php endwhile; ?>
        </div>
      <?php endif; ?>


      <?php
      	/**
      	 * Testimonail Query
      	 */
        $testimonial_query_args = array(
          'post_type'       => 'testimonial',
          'posts_per_page'  => -1,
          'order'           => 'ASC',
        );
        $testimonial_query = new WP_Query( $testimonial_query_args );
      ?>
      <?php if ( $testimonial_query->have_posts() ) : ?>

				<div class="l-grid-wrap">
          <div class="l-grid l-grid--three-col">

            <?php while ( $testimonial_query->have_posts() ) : $testimonial_query->the_post(); ?>

              <div class="l-grid-item">
                <?php get_template_part( 'template-parts/card-testimonial' ); ?>
              </div>

            <?php endwhile; ?>

          </div>
				</div>

      <?php endif; ?>

      <div class="back-top back-top--article vh">
        <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
      </div>

    </div>
  </section>

<?php get_footer(); ?>
