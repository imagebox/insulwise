<?php
/**
 * Displays the sidebar
 *
 * @package boxpress
 */
?>
<aside class="sidebar" role="complementary">

  <?php if ( is_page() ) :
      /**
       * Query for Child Pages
       * ---
       * If we've already queries for the child
       * pages, don't query again.
       */
      if ( ! isset( $child_pages_list )) {
        $child_pages_list = query_for_child_page_list();
      }
    ?>
    <?php if ( $child_pages_list ) : ?>

      <div class="sidebar-widget">
        <nav class="sidebar-nav">
          <ul>

            <?php echo $child_pages_list; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>
  <?php endif; ?>


  <?php // Blog Archive + Post ?>
  <?php if ( is_home() ||
             is_singular('post') ||
             ( is_archive() && get_post_type() == 'post' )) :

      /**
       * Blog Category Links
       */

      $blog_cats = wp_list_categories( array(
        'title_li'  => '',
        'echo'      => false,
      ));
    ?>

    <?php if ( $blog_cats ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Categories', 'boxpress'); ?></h4>
        <nav class="categories-widget">
          <ul>
            <li class="<?php
                if ( is_home() ) {
                  echo 'current-cat';
                }
              ?>">
              <a href="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
                <?php _e( 'All Posts', 'boxpress' ); ?>
              </a>
            </li>

            <?php echo $blog_cats; ?>

          </ul>
        </nav>
      </div>

    <?php endif; ?>

    <?php
      /**
       * Blog Archive Links
       */

      $blog_archives = wp_get_archives( array(
        'type'            => 'monthly',
        'format'          => 'option',
        'show_post_count' => 1,
        'echo'            => false,
      ));
    ?>

    <?php if ( $blog_archives ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Archives', 'boxpress'); ?></h4>
        <div class="archives-widget">
          <form action="<?php echo get_permalink( get_option( 'page_for_posts' )); ?>">
            <label class="vh" for="archive_dropdown"><?php _e('Select Year', 'boxpress'); ?></label>
            <select id="archive_dropdown" class="ui-select" name="archive_dropdown" onchange="document.location.href=this.options[this.selectedIndex].value;">
              <option value=""><?php echo _e( 'Select Month' ); ?></option>
              <?php echo $blog_archives; ?>
            </select>
          </form>
        </div>
      </div>

    <?php endif; ?>

    <?php
      /**
       * Blog Tag Links
       */

      $tags = get_tags( array(
        'orderby' => 'count',
        'order'   => 'DESC',
      ));
    ?>

    <?php if ( $tags ) : ?>

      <div class="sidebar-widget">
        <h4 class="widget-title"><?php _e('Tags', 'boxpress'); ?></h4>
        <div class="tags-widget">
          <ul>

            <?php foreach ( $tags as $tag ) : ?>

              <li>
                <a href="<?php echo get_tag_link( $tag->term_id ); ?>">
                  <?php echo $tag->name; ?>
                </a>
              </li>

            <?php endforeach; ?>

          </ul>
        </div>
      </div>

    <?php endif; ?>
  <?php endif; ?>

  <div class="latest-news-sidebar">
      <div class="message-box side-bar-callout-one">
        <h4>Get comfortable at home.</h4>
        <p>Insulwise can help!</p>
        <h4>412-214-9417</h4>
          <a class="button" href="<?php echo esc_url( site_url( '/contact/' )) ?>">Send Us A Message</a>
      </div>

      <div class="message-box side-bar-callout-two">
        <h4>Schedule a Free Home Energy - a $350 Value!</h4>
        <a class="button" href="<?php echo esc_url( site_url( '/free-energy-comfort-analysis-of-your-home/' )) ?>">Book Today</a>
      </div>
    <div class="latest-news-header">
      <h3>Latest News</h3>
    </div>
    <?php // Latest Posts Example ?>
    <?php
      $home_post_query_args = array(
        'post_type' => 'post',
        'posts_per_page' => 3
      );
      $home_post_query = new WP_Query( $home_post_query_args );
    ?>
    <div class="post-container">
      <?php if ( $home_post_query->have_posts() ) : ?>

            <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>



                  <div class="box">
                    <div class="box-header">
                      <h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                   <div class="box-footer">
                     <?php the_excerpt(); ?>
                     <a class="text-button" href="<?php echo get_the_permalink(); ?>"><?php _e('Read More', 'boxpress'); ?></a>
                   </div>
                  </div>

              <?php endwhile; ?>

        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </div>




</aside>
