<?php
/**
 * BoxPress functions and definitions
 *
 * @package boxpress
 */

if ( ! function_exists( 'boxpress_setup' )) :
function boxpress_setup() {

  add_theme_support( 'automatic-feed-links' );
  add_theme_support( 'title-tag' );
  add_theme_support( 'post-thumbnails', array( 'post', 'page' ));
  add_theme_support( 'html5', array(
    'search-form',
    'comment-form',
    'comment-list',
    'gallery',
    'caption',
  ));

  add_image_size( 'home_slideshow', 1200, 600, true );
  add_image_size( 'home_slideshow_big', 1200, 600, true );
  add_image_size( 'banner_image_size', 475, 185, true );
  add_image_size( 'home_index_thumb', 860, 350, true );
  add_image_size( 'slide_link_photo_size', 125, 85, true );
  add_image_size( 'logo_slideshow', 150, 150, true );
  add_image_size( 'card_thumb_size', 267, 138, true );
  add_image_size( 'shortcut_vid_thumb_size', 401, 170, true );
  add_image_size( 'shortcut_photo_size', 241, 138, true );
  add_image_size( 'block_full_width', 1600, 500, true );
  add_image_size( 'grey_bg_photo_size', 1600, 500, true );
  add_image_size( 'grey_bg_photo_size', 1600, 500, true );
  add_image_size( 'block_half_width', 800, 500, true );
}
endif;
add_action( 'after_setup_theme', 'boxpress_setup' );



/**
 * Enqueue scripts and styles.
 */

function boxpress_scripts() {

  /**
   * Styles
   */

  // Google Fonts - Replace with your fonts URL
  $style_font_url = 'https://fonts.googleapis.com/css?family=Montserrat:400,500,500i,600,600i,700,700i,900|Ubuntu:400,500,700';
  wp_enqueue_style( 'google-fonts', $style_font_url, array(), false, 'screen' );

  // Main screen styles
  $style_screen_path  = get_template_directory_uri() . '/assets/css/style.min.css';
  $style_screen_ver   = filemtime( get_template_directory() . '/assets/css/style.min.css' );
  wp_enqueue_style( 'screen', $style_screen_path, array('google-fonts'), $style_screen_ver, 'screen' );

  // Main print styles
  $style_print_path   = get_template_directory_uri() . '/assets/css/print.min.css';
  $style_print_ver    = filemtime( get_template_directory() . '/assets/css/print.min.css' );
  wp_enqueue_style( 'print', $style_print_path, array( 'screen' ), $style_print_ver, 'print' );


  /**
   * Scripts
   */

  // HTML5 Shiv - Only load for <IE9
  $script_html5shiv_path  = get_template_directory_uri() . '/assets/js/dev/html5shiv-printshiv.min.js';
  $script_html5shiv_ver   = filemtime( get_template_directory() . '/assets/js/dev/html5shiv-printshiv.min.js' );
  wp_enqueue_script( 'html5shiv', $script_html5shiv_path, array(), $script_html5shiv_ver, false );
  wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );

  // Main site scripts
  $script_site_path = get_template_directory_uri() . '/assets/js/build/site.min.js';
  $script_site_ver  = filemtime( get_template_directory() . '/assets/js/build/site.min.js' );
  wp_enqueue_script( 'site', $script_site_path, array( 'jquery' ), $script_site_ver, true );



  // Single Posts
  if ( is_singular() && comments_open() &&
       get_option( 'thread_comments' )) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'boxpress_scripts' );



/**
 * Query for child pages of current page
 *
 * @param  array  $options Accepts post type, depth and page ID.
 * @return string          Returns WP list of pages in html.
 */
function query_for_child_page_list( $options = array() ) {
  $default_options = array(
    'post_type' => 'page',
    'depth'     => 4,
    'page_id'   => '',
  );
  $config = array_merge( $default_options, $options );

  global $wp_query;
  $post = $wp_query->post;

  if ( $post ) {
    $parent_ID = $post->ID;

    if ( ! empty( $config['page_id'] )) {
      $parent_ID = $config['page_id'];
    }

    if ( $post->post_parent !== 0 ) {
      $ancestors  = get_post_ancestors( $post );
      $parent_ID  = end( $ancestors );
    }

    $list_pages_args = array(
      'post_type' => $config['post_type'],
      'title_li'  => '',
      'child_of'  => $parent_ID,
      'depth'     => $config['depth'],
      'echo'      => false,
    );

    // Get list of pages
    return wp_list_pages( $list_pages_args );
  }
}


/**
 * Numbered Pagination
 */
function boxpress_pagination( $current_query = array() ) {
  $format = '?paged=%#%';

  // Settings for using a main query
  if ( ! $current_query ) {
    global $wp_query;
    $current_query = $wp_query;
    $format = 'page/%#%/';
  }

  $max_num_pages  = $current_query->max_num_pages;
  $found_posts    = $current_query->found_posts;
  $posts_per_page = $current_query->posts_per_page;
  $big        = 999999999;
  $prev_arrow = is_rtl() ? '→' : '←';
  $next_arrow = is_rtl() ? '←' : '→';

  if ( $max_num_pages > 1 && $found_posts > $posts_per_page )  {
    if ( ! $current_page = get_query_var( 'paged' )) {
      $current_page = 1;
    }

    echo '<div class="pagination">';

    echo paginate_links(array(
      'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'format'    => $format,
      'current'   => max( 1, get_query_var( 'paged' )),
      'total'     => $max_num_pages,
      'mid_size'  => 3,
      'type'      => 'list',
      'prev_text' => $prev_arrow,
      'next_text' => $next_arrow,
    ));

    echo '</div>';
  }
}


/**
 * CPTs
 */
require get_template_directory() . '/inc/cpt/cpt-testimonial.php';

/**
 * Menu setup
 */
require get_template_directory() . '/inc/site-navigation.php';

/**
 * Custom Gallery
 */
require get_template_directory() . '/inc/custom-gallery.php';

/**
 * Accessibility
 */
require get_template_directory() . '/inc/walkers/class-aria-walker-nav-menu.php';

/**
 * Cleanup the header
 */
require get_template_directory() . '/inc/cleanup.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Admin Related Functions
 */
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/editor.php';

/**
 * ACF
 */
// Options Pages
require get_template_directory() . '/inc/acf-options.php';
// Search Queries
require get_template_directory() . '/inc/acf-search.php';

/**
 * Plugin Settings
 */
require get_template_directory() . '/inc/plugin-settings/gravity-forms-settings.php';
require get_template_directory() . '/inc/plugin-settings/tribe-events-settings.php';



/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 13;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );



add_filter('wp_nav_menu_items', 'my_wp_nav_menu_items', 10, 2);
function my_wp_nav_menu_items( $items, $args ) {
	// get menu
	$menu = wp_get_nav_menu_object($args->menu);
	// modify primary only
	if( $args->theme_location == 'choose_your_house' ||  $args->theme_location == 'insulation') {
		// vars
    $menu_box_copy_one = get_field('menu_box_copy_one', $menu);
    $menu_box_copy_two = get_field('menu_box_copy_two', $menu);
		// prepend logo
    $add_menu_box_copy_one = '<div class="box-copy">' .$menu_box_copy_one; '</div>';
    $add_menu_box_copy_two = '<div class="box-copy">' .$menu_box_copy_two; '</div>';
    $add_menu_items = '<div class="menu-item-container">' .$items; '</div>';
		// append html
    $items = $add_menu_box_copy_one .$add_menu_box_copy_two .$add_menu_items;
	}
	// return
	return $items;
}

add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args ) {
	// loop
	foreach( $items as $item ) {
		// vars
		$photo = get_field('link_photo', $item);
		// append icon
		if( $photo ) {
      $item->title = ' <img src="'.$photo['url'].'" alt="'.$photo['alt'].'" /> ' . $item->title;
		}
	}
	// return
	return $items;
}
