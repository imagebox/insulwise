<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-slideshow' ); ?>


    <?php // form ?>
    <section class="home-form gradient-red">
      <div class="wrap">
          <div class="request-demo-form">
             <?php gravity_form( 1, true, false, false, '', false ); ?>
           </div>
      </div>
    </section>

    <?php // shortcut cards?>
    <section class="section shortcut-card">

      <?php


      $grey_bg_photo  = get_field( 'grey_bg_photo' );
      $grey_bg_photo_size 	= 'grey_bg_photo_size';

       ?>

      <img class="bg-grey-photo" draggable="false" aria-hidden="true"
      src="<?php echo ( $grey_bg_photo['sizes'][ $grey_bg_photo_size ] ); ?>"
      width="<?php echo ( $grey_bg_photo['sizes'][ $grey_bg_photo_size . '-width'] ); ?>"
      height="<?php echo ( $grey_bg_photo['sizes'][ $grey_bg_photo_size . '-height'] ); ?>"
      alt="">

      <div class="wrap">

        <?php if( have_rows('shortcut_card_repeater') ): ?>
          <?php while ( have_rows ('shortcut_card_repeater') ) : the_row(); ?>

          <div class="l-grid l-grid--four-col shortcut-card-links">

            <?php if( have_rows('shortcut_card_row') ): ?>
              <?php while ( have_rows ('shortcut_card_row') ) : the_row();

              $card_icon        = get_sub_field( 'card_icon' );
              $card_title        = get_sub_field( 'card_title' );
              $card_vid_link        = get_sub_field( 'card_vid_link' );
              $card_thumb        = get_sub_field( 'card_thumb' );
              $card_link        = get_sub_field( 'card_link' );
              $card_thumb_size 	= 'card_thumb_size';
              ?>

              <div class="l-grid-item">
                <div class="card">
                  <div class="card-header">
                    <div class="card-icon">
                      	<img src="<?php echo $card_icon['url']; ?>" alt="<?php echo $card_icon['alt']; ?>" />
                    </div>
                  <?php echo $card_title; ?>
                  </div>

                <?php if ( ! empty($card_vid_link )) : ?>

                  <div class="card-thumbnail">
                    <a class="card-video-button" href="<?php echo esc_url($card_vid_link); ?>">
                      <img draggable="false" aria-hidden="true"
                      src="<?php echo esc_url( $card_thumb['sizes'][ $card_thumb_size ] ); ?>"
                      width="<?php echo esc_attr( $card_thumb['sizes'][ $card_thumb_size . '-width'] ); ?>"
                      height="<?php echo esc_attr( $card_thumb['sizes'][ $card_thumb_size . '-height'] ); ?>"
                      alt="">
                      <svg width="46px" height="46px" viewBox="0 0 46 46" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <defs>
                              <path d="M349.318246,1075.96084 C347.529878,1072.89681 345.103834,1070.47076 342.039795,1068.68239 C338.975116,1066.89402 335.629477,1066 332.000639,1066 C328.372121,1066 325.025843,1066.89402 321.961484,1068.68239 C318.897125,1070.47044 316.471081,1072.89649 314.682712,1075.96084 C312.894024,1079.0252 312,1082.3718 312,1086 C312,1089.6282 312.894344,1092.97448 314.682393,1096.03916 C316.470761,1099.10256 318.896805,1101.52924 321.961164,1103.31761 C325.025523,1105.10598 328.371802,1106 332.00032,1106 C335.628838,1106 338.975436,1105.10598 342.039475,1103.31761 C345.103514,1101.52956 347.529558,1099.10319 349.317927,1096.03916 C351.105976,1092.97448 352,1089.62788 352,1086 C352,1082.3718 351.105976,1079.02488 349.318246,1075.96084 L349.318246,1075.96084 Z M342.143217,1087.44063 L327.571335,1095.76625 C327.303385,1095.92208 327.017681,1096 326.714223,1096 C326.428519,1096 326.142815,1095.93071 325.857112,1095.79212 C325.285704,1095.46256 325,1094.97718 325,1094.33532 L325,1077.68473 C325,1077.04319 325.285704,1076.55749 325.857112,1076.22794 C326.446273,1075.91563 327.017681,1075.92425 327.571335,1076.2538 L342.143217,1084.5791 C342.714296,1084.89109 343,1085.36849 343,1086.01003 C343,1086.65156 342.714296,1087.12833 342.143217,1087.44063 L342.143217,1087.44063 Z" id="path-1"></path>
                              <filter x="-13.8%" y="-8.8%" width="127.5%" height="127.5%" filterUnits="objectBoundingBox" id="filter-2">
                                  <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                                  <feGaussianBlur stdDeviation="1.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                                  <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.310461957 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
                              </filter>
                          </defs>
                          <g id="Layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="Homepage-V5" transform="translate(-309.000000, -1065.000000)">
                                  <g id="Shape-Copy-4">
                                      <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
                                      <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>
                                  </g>
                              </g>
                          </g>
                      </svg>
                      <span>Watch Video</span>
                    </a>
                  </div>

                <?php elseif ( $card_link ) : ?>

                  <div class="card-thumbnail">
                    <a class="card-video-button"
                        href="<?php echo esc_url( $card_link['url'] ); ?>"
                        target="<?php echo esc_attr( $card_link['target'] ); ?>">
                          <img draggable="false" aria-hidden="true"
                          src="<?php echo esc_url( $card_thumb['sizes'][ $card_thumb_size ] ); ?>"
                          width="<?php echo esc_attr( $card_thumb['sizes'][ $card_thumb_size . '-width'] ); ?>"
                          height="<?php echo esc_attr( $card_thumb['sizes'][ $card_thumb_size . '-height'] ); ?>"
                          alt="">
                    </a>
                  </div>

                <?php endif; ?>

                  <div class="card-footer">
                    <a class="button"
                      href="<?php echo esc_url( $card_link['url'] ); ?>"
                      target="<?php echo esc_attr( $card_link['target'] ); ?>">
                      <?php echo $card_link['title']; ?>
                    </a>
                  </div>
                </div>
              </div>

              <?php endwhile; ?>
            <?php endif; ?>

          </div>

          <?php endwhile; ?>
        <?php endif; ?>

      </div>
    </section>



    <?php if( have_rows('shortcut_link_repeater') ): ?>
      <?php while ( have_rows ('shortcut_link_repeater') ) : the_row(); ?>

        <?php if( have_rows('shortcut_link_row') ): ?>
          <?php while ( have_rows ('shortcut_link_row') ) : the_row();

          $shortcut_link_1 = get_sub_field( 'shortcut_link_1' );
          $shortcut_link_2 = get_sub_field( 'shortcut_link_2' );
          $shortcut_link_heading_1 = get_sub_field('shortcut_link_heading_1');
          $shortcut_link_heading_2 = get_sub_field('shortcut_link_heading_2');
          $shortcut_vid_link        = get_sub_field( 'shortcut_vid_link' );
          $shortcut_vid_thumb = get_sub_field('shortcut_vid_thumb');
          $shortcut_vid_thumb_size 	= 'shortcut_vid_thumb_size';
          $shortcut_photo = get_sub_field('shortcut_photo');
          $shortcut_photo_size 	= 'shortcut_photo_size';

        ?>


        <section class="shortcut-link">
          <div class="block-1">
            <div class="content">
              <div class="content-text">
                <h3><?php echo $shortcut_link_heading_1; ?></h3>
                <a class="button"
                  href="<?php echo esc_url( $shortcut_link_1['url'] ); ?>"
                  target="<?php echo esc_attr( $shortcut_link_1['target'] ); ?>">
                  <?php echo $shortcut_link_1['title']; ?>
                </a>
              </div>
            <div class="photo">
              <img draggable="false" aria-hidden="true"
              src="<?php echo ( $shortcut_photo['sizes'][ $shortcut_photo_size ] ); ?>"
              width="<?php echo ( $shortcut_photo['sizes'][ $shortcut_photo_size . '-width'] ); ?>"
              height="<?php echo ( $shortcut_photo['sizes'][ $shortcut_photo_size . '-height'] ); ?>"
              alt="">
            </div>
            </div>
          </div>
           <div class="block-2">
            <div class="content">
              <h3><?php echo $shortcut_link_heading_2 ?></h3>
              <a class="button"
                href="<?php echo esc_url( $shortcut_link_2['url'] ); ?>"
                target="<?php echo esc_attr( $shortcut_link_2['target'] ); ?>">
                <?php echo $shortcut_link_2['title']; ?>
              </a>
            </div>
            <div class="content-video">
            <a class="shortcut-video-button" href="<?php echo esc_url($shortcut_vid_link); ?>">
              <svg width="46px" height="46px" viewBox="0 0 46 46" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <defs>
                      <path d="M349.318246,1075.96084 C347.529878,1072.89681 345.103834,1070.47076 342.039795,1068.68239 C338.975116,1066.89402 335.629477,1066 332.000639,1066 C328.372121,1066 325.025843,1066.89402 321.961484,1068.68239 C318.897125,1070.47044 316.471081,1072.89649 314.682712,1075.96084 C312.894024,1079.0252 312,1082.3718 312,1086 C312,1089.6282 312.894344,1092.97448 314.682393,1096.03916 C316.470761,1099.10256 318.896805,1101.52924 321.961164,1103.31761 C325.025523,1105.10598 328.371802,1106 332.00032,1106 C335.628838,1106 338.975436,1105.10598 342.039475,1103.31761 C345.103514,1101.52956 347.529558,1099.10319 349.317927,1096.03916 C351.105976,1092.97448 352,1089.62788 352,1086 C352,1082.3718 351.105976,1079.02488 349.318246,1075.96084 L349.318246,1075.96084 Z M342.143217,1087.44063 L327.571335,1095.76625 C327.303385,1095.92208 327.017681,1096 326.714223,1096 C326.428519,1096 326.142815,1095.93071 325.857112,1095.79212 C325.285704,1095.46256 325,1094.97718 325,1094.33532 L325,1077.68473 C325,1077.04319 325.285704,1076.55749 325.857112,1076.22794 C326.446273,1075.91563 327.017681,1075.92425 327.571335,1076.2538 L342.143217,1084.5791 C342.714296,1084.89109 343,1085.36849 343,1086.01003 C343,1086.65156 342.714296,1087.12833 342.143217,1087.44063 L342.143217,1087.44063 Z" id="path-1"></path>
                      <filter x="-13.8%" y="-8.8%" width="127.5%" height="127.5%" filterUnits="objectBoundingBox" id="filter-2">
                          <feOffset dx="0" dy="2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                          <feGaussianBlur stdDeviation="1.5" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                          <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.310461957 0" type="matrix" in="shadowBlurOuter1"></feColorMatrix>
                      </filter>
                  </defs>
                  <g id="Layout" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                      <g id="Homepage-V5" transform="translate(-309.000000, -1065.000000)">
                          <g id="Shape-Copy-4">
                              <use fill="black" fill-opacity="1" filter="url(#filter-2)" xlink:href="#path-1"></use>
                              <use fill="#FFFFFF" fill-rule="evenodd" xlink:href="#path-1"></use>
                          </g>
                      </g>
                  </g>
              </svg>
              <img draggable="false" aria-hidden="true"
              src="<?php echo esc_url( $shortcut_vid_thumb['sizes'][ $shortcut_vid_thumb_size ] ); ?>"
              width="<?php echo esc_attr( $shortcut_vid_thumb['sizes'][ $shortcut_vid_thumb_size . '-width'] ); ?>"
              height="<?php echo esc_attr( $shortcut_vid_thumb['sizes'][ $shortcut_vid_thumb_size . '-height'] ); ?>"
              alt="">
            </a>
            </div>
          </div>
        </section>


          <?php endwhile; ?>
        <?php endif; ?>

      <?php endwhile; ?>
    <?php endif; ?>

<?php

  $welcome_message_header = get_field('welcome_message_header');
  $welcome_message = get_field('welcome_message');

?>

<section class="welcome section">
    <?php

    $welcome_grey_bg_photo  = get_field( 'welcome_grey_bg_photo' );
    $welcome_grey_bg_photo_size 	= 'grey_bg_photo_size';

     ?>

    <img draggable="false" aria-hidden="true"
    src="<?php echo ( $welcome_grey_bg_photo['sizes'][ $welcome_grey_bg_photo_size ] ); ?>"
    width="<?php echo ( $welcome_grey_bg_photo['sizes'][ $welcome_grey_bg_photo_size . '-width'] ); ?>"
    height="<?php echo ( $welcome_grey_bg_photo['sizes'][ $welcome_grey_bg_photo_size . '-height'] ); ?>"
    alt="">

  <div class="wrap">
    <div class="welcome-message-container">
     <div class="welcome-message">
       <h3><?php echo $welcome_message_header; ?></h3>
       <p><?php echo $welcome_message; ?></p>
     </div>
     <div class="latest-news">
       <h3>Latest News</h3>
       <?php // Latest Posts Example ?>
       <?php
         $home_post_query_args = array(
           'post_type' => 'post',
           'posts_per_page' => 3,
           'orderby' => 'date',
           'order' => 'DESC',
         );
         $home_post_query = new WP_Query( $home_post_query_args );
       ?>
       <?php if ( $home_post_query->have_posts() ) : ?>

             <?php while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>



                   <div class="box">
                     <div class="box-date">
                      <?php

                      echo get_the_date('M');
                      echo '<br />';
                      echo get_the_date('y');

                       ?>
                     </div>
                     <div class="box-header">
                       <h3><a href="<?php echo get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
                     </div>
                    <div class="box-footer">
                      <?php the_excerpt(); ?>
                      <a class="text-button" href="<?php echo get_the_permalink(); ?>"><?php _e('Read More', 'boxpress'); ?></a>
                    </div>
                   </div>

               <?php endwhile; ?>

         <?php wp_reset_postdata(); ?>
       <?php endif; ?>
     </div>
    </div>
  </div>
</section>


<section class="section carousel-section">
  <div class="wrap">

    <?php if( have_rows('logo_repeater') ): ?>
      <?php while ( have_rows ('logo_repeater') ) : the_row();

      $logo_header = get_sub_field('logo_header');

      ?>

      <div class="logo-header">
        <h3><?php echo $logo_header; ?></h3>
      </div>

      <div class="logo-carousel">

        <?php if( have_rows('logo_repeater_nested') ): ?>
          <?php while ( have_rows ('logo_repeater_nested') ) : the_row();

          $logo_item = get_sub_field('logo_item');
          $logo_slideshow = 'logo_slideshow';
          $logo_link  = get_sub_field( 'logo_link' );

          ?>

          <div class="slide">

            <?php if ( $logo_item ) : ?>
               <?php if ( $logo_link ) : ?>
                 <a href="<?php echo $logo_link['url']; ?>" target="<?php echo $logo_link['target']; ?>">
               <?php endif; ?>

                <img src="<?php echo esc_url( $logo_item['url'] ); ?>"
                width="<?php echo esc_attr( $logo_item['width'] ); ?>"
                height="<?php echo esc_attr( $logo_item['height'] ); ?>"
                alt="<?php echo esc_attr( $logo_item['alt'] ); ?>">

                <?php if ( $logo_link ) : ?>
                  </a>
                <?php endif; ?>
              <?php endif; ?>

          </div>

          <?php endwhile; ?>
        <?php endif; ?>

      </div>
        <?php endwhile; ?>
      <?php endif; ?>
  </div>
</section>


  </article>

<?php get_footer(); ?>
